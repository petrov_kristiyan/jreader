package manga.com.jreader.mangaList.chaptersList.chapter.list.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.widget.TextView;

import java.util.List;

import manga.com.jreader.R;
import manga.com.jreader.model.Chapter;

public class CustomChapterListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Chapter> items;
    private String mangaName;

    public CustomChapterListAdapter(Activity activity, List<Chapter> items,String mangaName) {
        this.activity = activity;
        this.items = items;
        this.mangaName =mangaName;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.capters_list_row, null);
        final Chapter chap = items.get(position);
        TextView name = (TextView) convertView.findViewById(R.id.name);

        TextView date = (TextView) convertView.findViewById(R.id.date);
        date.setText(chap.getDate());

        name.setText(chap.getName().replace(mangaName," Chapter "));
        return convertView;
    }


}
