package manga.com.jreader.mangaList.viewPager.fragment.list.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import manga.com.jreader.R;
import manga.com.jreader.model.Manga;
import uk.co.senab.photoview.PhotoView;

public class MangaListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Manga> items;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public MangaListAdapter(Activity activity, List<Manga> items) {
        this.activity = activity;
        this.items = items;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).resetViewBeforeLoading(true).showImageOnFail(R.drawable.error).build();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.manga_list_row, null);
        final Manga m = items.get(position);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        LinearLayout newChapterListLayout =(LinearLayout) convertView.findViewById(R.id.newChapterList);
        newChapterListLayout.removeAllViews();
        for(int i=0;i<m.getChapters().size();i++){

            TextView chapter = new TextView(activity);
            chapter.setText(m.getChapters().get(i));

            newChapterListLayout.addView(chapter);
        }
        TextView date = (TextView) convertView.findViewById(R.id.releaseDate);
        date.setText(m.getDate());
        PhotoView thumbNail = (PhotoView) convertView.findViewById(R.id.thumbnail);
        imageLoader.displayImage(m.getThumbImageURL(), thumbNail, options);
        title.setText(m.getTitle());


        return convertView;
    }


}
