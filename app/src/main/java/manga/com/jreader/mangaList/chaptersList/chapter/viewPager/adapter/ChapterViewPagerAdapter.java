package manga.com.jreader.mangaList.chaptersList.chapter.viewPager.adapter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import manga.com.jreader.mangaList.chaptersList.chapter.viewPager.fragment.MangaPageFragment;

public class ChapterViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<String> chapters;

    public ChapterViewPagerAdapter(FragmentManager fragmentManager, ArrayList<String> chapters) {
        super(fragmentManager);
        this.chapters = chapters;
    }

    @Override
    public int getCount() {
        return chapters.size();
    }

    @Override
    public Fragment getItem(int position) {
        MangaPageFragment mangaPageFragment = new MangaPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("PAGE", chapters.get(position));
        mangaPageFragment.setArguments(bundle);
        return mangaPageFragment;
    }
}

