package manga.com.jreader.mangaList.chaptersList;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import manga.com.jreader.R;
import manga.com.jreader.mangaList.chaptersList.chapter.ChapterActivity;
import manga.com.jreader.downloadTasks.DownloadListChapters;
import manga.com.jreader.downloadTasks.ICallBack;
import manga.com.jreader.mangaList.chaptersList.chapter.list.adapter.CustomChapterListAdapter;
import manga.com.jreader.model.Chapter;

public class ChaptersListActivity extends ActionBarActivity implements ICallBack<String,Chapter> {
    private ListView listView;
    private ArrayList<Chapter> chapters;
    private CustomChapterListAdapter adapter;
    private String mangaName;
    private DownloadListChapters d;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chapters_list);
        chapters = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listChapters);
        Bundle bundle = getIntent().getExtras();
        chapters = new ArrayList<>();
        String id = "";
        if (bundle != null) {
            id = bundle.getString("ID");
            mangaName = bundle.getString("NAME");
            setTitle(mangaName);
        }
        d = new DownloadListChapters(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            d.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id);
        } else {
            d.execute(id);
        }
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(String result) {
        adapter = new CustomChapterListAdapter(ChaptersListActivity.this,chapters,mangaName);
        listView.setAdapter(adapter);
        final ArrayList<String> chaptersUrls= new ArrayList<>();
        for(Chapter c: chapters){
            chaptersUrls.add(c.getUrl());
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, final int position, final long l) {
                Intent intent = new Intent(ChaptersListActivity.this, ChapterActivity.class);
                intent.putExtra("CHAPTER", position);
                intent.putExtra("CHAPTERS",chaptersUrls);
                startActivity(intent);
            }
        });
        findViewById(R.id.progressBar2).setVisibility(View.GONE);

    }

    @Override
    public void taskProgress(Chapter... progress) {
        chapters.add(progress[0]);
    }

    @Override
    public void taskCancelled(int val, int size) {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
