package manga.com.jreader.mangaList;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import com.shamanland.fab.FloatingActionButton;

import manga.com.jreader.R;
import manga.com.jreader.mangaList.viewPager.adapter.ViewPagerAdapter;

public class MainActivity extends ActionBarActivity {
    private ViewPagerAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.pagerPages);
        String url = "http://manga-joy.com/";

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Integer.MAX_VALUE, url);

        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int i) {
                i++;
                adapter.stopDownload(i);
            }

        });

        viewPager.setAdapter(adapter);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.searchButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.stopDownload(-1);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
