package manga.com.jreader.mangaList.chaptersList.chapter.viewPager.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;


import manga.com.jreader.R;
import manga.com.jreader.downloadTasks.DownloadPage;
import manga.com.jreader.downloadTasks.ICallBack;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;


public class MangaPageFragment extends Fragment implements ICallBack<String,String> {
    private View layoutView;
    private ProgressBar progressBar;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private static PhotoView imageView;
    private TextView title;
    private String chapTitle;
    private String chapPage;
    private TextView page;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         imageLoader = ImageLoader.getInstance();
         options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).resetViewBeforeLoading(true).showImageOnFail(R.drawable.error).build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutView = inflater.inflate(R.layout.manga_page, container, false);
        progressBar = (ProgressBar) layoutView.findViewById(R.id.progressBar2);
        title= (TextView) layoutView.findViewById(R.id.chapTitle);
        page= (TextView) layoutView.findViewById(R.id.chapPage);
        String urlPage = getArguments().getString("PAGE");

        DownloadPage download = new DownloadPage(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, urlPage);
        } else {
            download.execute(urlPage);
        }

        return layoutView;
    }

    public void updateImage(String url) {
        imageView= (PhotoView)layoutView.findViewById(R.id.image);
        imageLoader.displayImage(url, imageView,options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);
            }
        });
        title.setText(chapTitle);
        page.setText("Page : "+chapPage);
        imageView.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float v, float v2) {
                  if(title.getVisibility()== View.GONE){
                      title.setVisibility(View.VISIBLE);
                      page.setVisibility(View.VISIBLE);
                  }
                    else{
                      title.setVisibility(View.GONE);
                      page.setVisibility(View.GONE);
                  }
            }
        });
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(String result) {

    }

    @Override
    public void taskProgress(String... progress) {
        chapTitle=progress[1];
        chapPage=progress[2];
        updateImage(progress[0]);
    }

    @Override
    public void taskCancelled(int val, int size) {

    }
}
