package manga.com.jreader.mangaList.chaptersList.chapter.viewPager.viewpager;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ChapterViewPager extends ViewPager {

    float mStartDragX;
    OnSwipeOutListener mListener;
    static final int MIN_DISTANCE = 100;
    private float downX, downY, upX, upY, upX2;

    public ChapterViewPager(Context context) {
        super(context);
    }

    public ChapterViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnSwipeOutListener(OnSwipeOutListener listener) {
        mListener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try{
        final int action = event.getActionMasked();
        switch (action & MotionEventCompat.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                mStartDragX = event.getX();
                if ((upX2 < mStartDragX) && getCurrentItem() == 0) {
                    mListener.onSwipeOutAtStart();
                    mStartDragX = 0;
                } else if ((upX2 > mStartDragX) && getCurrentItem() == getAdapter().getCount() - 1) {
                    mListener.onSwipeOutAtEnd();
                    mStartDragX = 0;
                }
                break;
        }
        return super.onTouchEvent(event);
    }catch(IllegalArgumentException ex){

    }
    return false;

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
      try {
          final int action = event.getActionMasked();
          switch (action & MotionEventCompat.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                upX2 = event.getX();
                downY = event.getY();
                break;
            }
            case MotionEvent.ACTION_UP: {
                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                // swipe horizontal?
                if(Math.abs(deltaX) < Math.abs(deltaY))
                {
                    if(Math.abs(deltaY) > MIN_DISTANCE){
                        // top or down
                        if(deltaY < 0) { mListener.onSwipeDown();

                        }
                        if(deltaY > 0) { mListener.onSwipeUp(); }
                        upX=0;
                        upY=0;
                    }
                }
            }
            break;
        }
          return super.onInterceptTouchEvent(event);
      }catch(IllegalArgumentException ex){

      }
        return false;
    }

    public interface OnSwipeOutListener {
        public void onSwipeOutAtStart();
        public void onSwipeOutAtEnd();
        public void onSwipeDown();
        public void onSwipeUp();
    }
}