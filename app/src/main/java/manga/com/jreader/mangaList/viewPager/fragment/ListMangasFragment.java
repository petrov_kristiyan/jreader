package manga.com.jreader.mangaList.viewPager.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import manga.com.jreader.mangaList.chaptersList.ChaptersListActivity;
import manga.com.jreader.R;
import manga.com.jreader.downloadTasks.DownloadListMangas;
import manga.com.jreader.downloadTasks.ICallBack;
import manga.com.jreader.mangaList.viewPager.adapter.ViewPagerAdapter;
import manga.com.jreader.mangaList.viewPager.fragment.list.adapter.MangaListAdapter;
import manga.com.jreader.model.Manga;


public class ListMangasFragment extends Fragment implements ICallBack<String,Manga> {

    private List<Manga> listManga = new ArrayList<>();
    private MangaListAdapter adapter;
    private ListView mangaList;
    private int page = 1;
    private View layoutView;
    private DownloadListMangas d;
    private String url;
    private int val = 0;
    private ProgressBar progressBar;
    private TextView textView;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutView = inflater.inflate(R.layout.manga_list, container, false);

        progressBar = (ProgressBar) layoutView.findViewById(R.id.progressBar);
        textView = (TextView)layoutView.findViewById(R.id.error_manga_list);
        mangaList = (ListView)layoutView.findViewById(R.id.mangaList);
        adapter = new MangaListAdapter(getActivity(), listManga);
        mangaList.setAdapter(adapter);
        mangaList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, final int position, final long l) {
                Intent intent = new Intent(getActivity(), ChaptersListActivity.class);
                intent.putExtra("NAME", listManga.get(position).getTitle());
                intent.putExtra("ID", listManga.get(position).getId());
                startActivity(intent);
                if (viewPagerAdapter != null)
                    viewPagerAdapter.stopDownload(-1);
            }
        });

        return layoutView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            url = getArguments().getString("URL");
            page = getArguments().getInt("PAGE");
            page++;
            d = new DownloadListMangas(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                d.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,val+"",url,+listManga.size()+"",page+"");
            } else {
                d.execute(val+"",url,+listManga.size()+"",page+"");
            }
        }
    }

    public int getPage() {
        page = getArguments().getInt("PAGE");
        page++;
        return page;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (d != null && d.isCancelled()) {
            url = getArguments().getString("URL");
            page = getArguments().getInt("PAGE");
            page++;
            d = new DownloadListMangas(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                d.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, val+"",url,+listManga.size()+"",page+"");
            } else {
                d.execute(val+"",url,+listManga.size()+"",page+"");
            }
        }
    }

    public void stopD() {
        if (d != null)
            d.cancel(true);
    }

    @Override
    public void taskCancelled(int val,int size) {
        this.val = val;
        if (size == 0 && getActivity() != null) {
            progressBar.setVisibility(View.INVISIBLE);
            textView.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetInvalidated();
    }

    @Override
    public void taskStart() {
    }

    @Override
    public void taskCompleted(String result) {
        val++;
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void taskProgress(Manga... progress) {
        listManga.add(progress[0]);
        adapter.notifyDataSetChanged();
    }

    public void setViewPagerAdapter(ViewPagerAdapter viewPagerAdapter) {
        this.viewPagerAdapter = viewPagerAdapter;
    }
}
