package manga.com.jreader.mangaList.viewPager.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import manga.com.jreader.mangaList.viewPager.fragment.ListMangasFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private int ITEMS; //number of pages
    private String url;
    private List<ListMangasFragment> listMangaFragments;

    public ViewPagerAdapter(FragmentManager fragmentManager, int pages, String url) {
        super(fragmentManager);
        ITEMS = pages;
        listMangaFragments = new ArrayList<>();
        this.url = url;
    }

    @Override
    public int getCount() {
        return ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        ListMangasFragment listMangasFragment = new ListMangasFragment();
        listMangasFragment.setViewPagerAdapter(this);
        Bundle bundle = new Bundle();
        bundle.putInt("PAGE", position);
        bundle.putString("URL", url);
        this.listMangaFragments.add(listMangasFragment);
        listMangasFragment.setArguments(bundle);
        return listMangasFragment;
    }

    public void stopDownload(int page) {
        for (ListMangasFragment listMangasFragment : this.listMangaFragments)
            if (listMangasFragment.getPage() != page) {
                listMangasFragment.stopD();
            }
    }

}
