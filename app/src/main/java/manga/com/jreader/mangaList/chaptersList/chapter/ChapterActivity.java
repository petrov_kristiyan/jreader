package manga.com.jreader.mangaList.chaptersList.chapter;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import java.util.ArrayList;

import manga.com.jreader.R;
import manga.com.jreader.downloadTasks.DownloadChapter;
import manga.com.jreader.downloadTasks.ICallBack;
import manga.com.jreader.mangaList.chaptersList.chapter.viewPager.viewpager.ChapterViewPager;
import manga.com.jreader.mangaList.chaptersList.chapter.viewPager.adapter.ChapterViewPagerAdapter;

public class ChapterActivity extends FragmentActivity implements ICallBack<String,String>{
    private ArrayList<String> chapters;
    private ChapterViewPagerAdapter adapter;
    private ArrayList<String> pagesChapters;
    private ChapterViewPager viewPager;
    private DownloadChapter download;
    private int chap;
    private boolean enter=false;
    private int revertPages = 0;
    private int nextChap;
    private int prevChap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chapter_viewpager);
        pagesChapters = new ArrayList<>();
        Bundle extra = getIntent().getExtras();
        chapters = extra.getStringArrayList("CHAPTERS");
        chap = extra.getInt("CHAPTER");
        prevChap = chap;
        nextChap = chap;
        viewPager = (ChapterViewPager) findViewById(R.id.pager);

        adapter = new ChapterViewPagerAdapter(getSupportFragmentManager(), pagesChapters);
        viewPager.setAdapter(adapter);

        viewPager.setOnSwipeOutListener(new ChapterViewPager.OnSwipeOutListener() {
            @Override
            public void onSwipeOutAtStart() {
                if (nextChap < chapters.size() - 1) {
                    nextChap++;
                    download = new DownloadChapter(ChapterActivity.this);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chapters.get(nextChap), "revert","");
                    } else {
                        download.execute(chapters.get(nextChap), "revert", nextChap + "");
                    }
                }
                else{
                    Toast.makeText(ChapterActivity.this,"There isn't any previous chapter",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSwipeOutAtEnd() {
                enter=true;
                if (prevChap >= 1) {
                    prevChap--;
                    download = new DownloadChapter(ChapterActivity.this);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chapters.get(prevChap), "","");
                    } else {
                        download.execute(chapters.get(prevChap), "", "");
                    }
                }
                else{
                    Toast.makeText(ChapterActivity.this,"This was the last chapter",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSwipeDown() {

            }

            @Override
            public void onSwipeUp() {


            }
        });
        download = new DownloadChapter(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chapters.get(chap), "", "");
        } else {
            download.execute(chapters.get(chap), "","");
        }
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(String result) {
        if (result != null ) {
            revertPages = Integer.parseInt(result);
            if (revertPages == 0) {
                adapter = new ChapterViewPagerAdapter(getSupportFragmentManager(), pagesChapters);
                viewPager.setAdapter(adapter);
            }
            viewPager.setCurrentItem(revertPages, false);
        }
    }

    @Override
    public void taskProgress(String... p) {
        if (p[1].equals("revert")) {
            pagesChapters.add(0, p[0]);
        } else {
            pagesChapters.add(p[0]);
        }
        adapter.notifyDataSetChanged();
        if(enter){
            enter=false;
            viewPager.setCurrentItem(adapter.getCount() - 1, false);
        }

    }

    @Override
    public void taskCancelled(int val, int size) {

    }
}