package manga.com.jreader.model;

import java.util.ArrayList;

public class Manga {


    private String id;
    private String title;
    private ArrayList<String> chapters;
    private String date;
    private String thumbImageURL;

    public Manga(String id, String title, ArrayList<String> chapters, String date, String thumbImageURL) {
        this.id = id;
        this.title = title;
        this.chapters = chapters;
        this.date = date;
        this.thumbImageURL = thumbImageURL;
    }

    public String getId() {
        return id;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public void setThumbImageURL(String thumbImageURL) {
        this.thumbImageURL = thumbImageURL;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<String> chapters) {
        this.chapters = chapters;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

