package manga.com.jreader.model;

public class Chapter {
    private String date;
    private String url;
    private String name;

    public Chapter(String date, String url, String name) {
        this.date = date;
        this.url = url;
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
