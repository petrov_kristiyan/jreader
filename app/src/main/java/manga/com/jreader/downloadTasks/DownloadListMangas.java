package manga.com.jreader.downloadTasks;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import manga.com.jreader.model.Manga;

public class DownloadListMangas extends AsyncTask<String, Manga, String> {

    private ICallBack<String, Manga> callBack;
    private int val=0;
    private int size = 0;

    public DownloadListMangas(ICallBack<String, Manga> callBack) {
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... params) {
            Document listMangaD;
            try {
                String url;
                if(params[3].equals("1")){
                     url = params[1];
                }
                else
                 url=params[1]+"latest-chapters/"+params[3]+"/";
                listMangaD = Jsoup.connect(url).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(180000).ignoreHttpErrors(true).followRedirects(true).get();
                    int sizeList= Integer.parseInt(params[2]);
                    Elements mangaList = listMangaD.getElementsByClass("row");
                    size = mangaList.size();
                    if (size > 0 && size > sizeList)
                        for (int i = Integer.parseInt(params[0]); i < mangaList.size(); i++) {
                            val = i;
                            if (isCancelled()) {
                                break;
                            }
                            Element e = mangaList.get(i);
                            String id = e.getElementsByTag("a").get(0).attr("href").replace(params[1], "").replace("/", "");
                            String title = e.getElementsByClass("ttl").get(0).ownText();
                            String urlI = e.getElementsByTag("img").get(0).attr("src").replace("30x0", "198x0");
                            Elements chaptersElements = e.getElementsByTag("ul").get(0).getElementsByTag("li");
                            ArrayList<String> chapters = new ArrayList<>();
                            String date = chaptersElements.get(0).getElementsByClass("dte").get(0).ownText();
                            for (Element chap : chaptersElements) {
                                chapters.add(chap.getElementsByClass("val").get(0).ownText());

                            }
                            Manga manga1 = new Manga(id, title, chapters, date, urlI);
                            publishProgress(manga1);
                        }
                    else {
                        cancel(true);

                    }


            } catch (IOException e) {
                e.printStackTrace();
            }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callBack.taskStart();
    }

    @Override
    protected void onPostExecute(String result) {
        callBack.taskCompleted(result);

    }

    @Override
    protected void onCancelled() {
        callBack.taskCancelled(val,size);

    }

    @Override
    protected void onProgressUpdate(Manga... values) {
        callBack.taskProgress(values);
    }

}