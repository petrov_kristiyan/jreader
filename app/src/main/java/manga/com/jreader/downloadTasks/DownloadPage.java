package manga.com.jreader.downloadTasks;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class DownloadPage extends AsyncTask<String, String, String> {

    private ICallBack<String,String> callBack;

    public DownloadPage(ICallBack<String,String> callBack){
        this.callBack=callBack;
    }

    @Override
    protected String doInBackground(String... url) {
        Document document;
        try {
            document = Jsoup.connect(url[0]).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(180000).ignoreHttpErrors(true).followRedirects(true).get();
            Element table = document.getElementsByClass("prw").get(0).getElementsByTag("tr").get(0);
            Element image = table.getElementsByTag("td").get(1).getElementsByTag("img").get(0);
            Elements chapters = document.getElementsByClass("wpm_nav_rdr").get(0).getElementsByTag("select");
            String chapTitle = chapters.get(0).getElementsByAttribute("selected").get(0).ownText();
            Elements pages = chapters.get(1).getElementsByTag("option");
            String chapPage = chapters.get(1).getElementsByAttribute("selected").get(0).ownText() + "/" + pages.get(pages.size() - 1).ownText();
            publishProgress(image.attr("src"),chapTitle,chapPage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onProgressUpdate(String... p) {
        callBack.taskProgress(p[0],p[1],p[2]);
    }

}