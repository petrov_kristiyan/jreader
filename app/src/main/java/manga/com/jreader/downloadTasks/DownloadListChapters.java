package manga.com.jreader.downloadTasks;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import manga.com.jreader.model.Chapter;

public class DownloadListChapters extends AsyncTask<String,Chapter,String>{

    private ICallBack<String, Chapter> callBack;

    public DownloadListChapters(ICallBack<String, Chapter> callBack){
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String url = "http://manga-joy.com/";
            Document manga = Jsoup.connect(url + "" + params[0] + "/").userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(180000).ignoreHttpErrors(true).followRedirects(true).get();
            Elements listChapter = manga.getElementsByClass("chp_lst").get(0).getElementsByTag("li");
            for (Element e2 : listChapter) {
                String urlChap = e2.getElementsByTag("a").get(0).attr("href");
                String name = e2.getElementsByClass("val").get(0).ownText();
                String date = e2.getElementsByClass("dte").get(0).ownText();
                Chapter chap = new Chapter(date,urlChap,name);
                publishProgress(chap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Chapter... values) {
        callBack.taskProgress(values[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.taskCompleted(s);
    }

}
