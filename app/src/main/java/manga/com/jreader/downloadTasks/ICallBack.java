package manga.com.jreader.downloadTasks;

public interface ICallBack<String,Object> {

    void taskStart();
    void taskCompleted(String result);
    void taskProgress(Object... progress);
    void taskCancelled(int val, int size);
}
