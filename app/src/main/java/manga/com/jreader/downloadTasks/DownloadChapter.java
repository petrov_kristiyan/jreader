package manga.com.jreader.downloadTasks;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class DownloadChapter extends AsyncTask<String,String,String> {

    private ICallBack<String,String> callBack;

    public DownloadChapter(ICallBack<String,String> callBack){
        this.callBack=callBack;
    }

    @Override
    protected String doInBackground(String... url) {
        Document document;
        String revertPages=null;
        try {
            if (url[1].equals("revert")) {
                document = Jsoup.connect(url[0]).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(180000).ignoreHttpErrors(true).followRedirects(true).get();
                Elements list = document.getElementsByClass("lnk_lst").get(0).getElementsByTag("a");
                if(!url[2].equals("firstPage"))
                    revertPages = (list.size() - 2)+"";
                else
                    revertPages="0";
                for(int k=list.size()-2;k>=0;k--)
                    publishProgress(list.get(k).attr("href"),"revert");
            } else {
                document = Jsoup.connect(url[0]).get();
                Elements list = document.getElementsByClass("lnk_lst").get(0).getElementsByTag("a");
                for(int k=0;k<list.size()-1;k++)
                    publishProgress(list.get(k).attr("href"),"");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return revertPages;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        callBack.taskProgress(values);
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.taskCompleted(s);
    }
}
